import { Component, OnInit } from '@angular/core';
import { StockService } from '../services/stock.service';
import { Observable } from 'rxjs';
import * as  moment from 'moment/moment.js';


@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {
    companiesStock$: Observable<any>;
    apiData: any = [];
    fluctuation: number;    /* fluctuations -+10% */
    renderData: any = [];
    constructor(private service: StockService) { }
    /**
     * initialized  method
     */
  ngOnInit() {
      this.companiesStock$ = this.service.GET_COMPANIES_STOCK();
      this.companiesStock$.subscribe((data: any[]) => {
          if (data) {
            this.apiData = data;
            this.dataMapping(this.apiData);
          }
      });
  }
    /**
     * dataMapping method
     */
    dataMapping(items, day = 1) {
        const companiesStocks = [];
        const currentDay = day;
        items.forEach((e: any) => {
            if (e) {
                this.fluctuation = this.randomfluctuation(-10, 10);
                const changeInValue = this.calculateChange(e.price , this.fluctuation );
                const currentValue = (e.price + changeInValue);
                companiesStocks.push({
                    name: e.name,
                    symbol: e.symbol,
                    price: e.price,
                    current_value: currentValue,
                    absolute_change: changeInValue,
                    percentage_change: this.fluctuation,
                    text_color_class: this.fluctuation >= 0 ? 'green' : 'red'
                });
            }
        });
        this.renderData.unshift({
            companiesData : companiesStocks,
            day : currentDay ,
            date : this.getDate(currentDay),
        });
    }
    /**
     * pass day method when click on button
     */
    passDay(day) {

        this.dataMapping(this.apiData, day);
    }
    /**
     * randomfluctuation method get ramdom fluctuation
     */
    randomfluctuation(min, max) { /*min and max included*/
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    /**
     * getDate method get formated current date and  after adding
     */
    getDate(currentDay) {
        const now = moment();
        if (currentDay > 1) {
            return now.add(currentDay - 1, 'days').format('dddd, MMMM DD, YYYY');
        } else {
            return now.format('dddd, MMMM DD, YYYY');
        }
    }
    /**
     * calculateChange method get Calculated Change price as per fluctuation
     */
    calculateChange(intialPrice = 0 , fluctuation = 0 , changeType = '') {

            return  ((intialPrice / 100) * fluctuation);
    }
}
