import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';

const API_GET_COMPANIES_STOCK = `https://staging-api.brainbase.com/stocks.php`;

@Injectable({
    providedIn: 'root'
})
export class StockService {
    constructor(private http: HttpClient) {}

    GET_COMPANIES_STOCK(): Observable<any> {

        return this.http.get(API_GET_COMPANIES_STOCK);
    }
}
